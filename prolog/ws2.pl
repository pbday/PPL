/**
*Question 1: using only a predicate father (a,b), define brother, cousin, grandson and descendent
*/

%Test predicates

father(a,b).
father(a,c).
father(b,d).
father(b,e).
father(c,f).


%The tree only contains men so we do not need to consider gender

brother(A,B) :- father(X,A), father(X,B) , A \== B.
cousin(A,B) :- father(X,A), father(Y,B), brother(X,Y).
grandson(A,B) :- father(X,A), father(B,X).

descendent(A,B) :- father(B,A).
descendent(A,B) :- father(X,A), descendent(X,B). 


/**
* Part e)
* Brother:
	* will generate using father(a,b), then a,c saying b and c are brothers.
	* then it will take a,c as the starting point, match it with a,b and return c and b are brothers
	* then say d,e followed by e,d.
* cousin:
	* after working throug hthe chanin, will be d,f then e,f , then f,d then f,e 
* grandson:
	* first time the first father term has the grandson in it is d, so d,a then e,a then f,a
*descendent:
	* firstly will display all in the order presented in father predicate list.
	* then  d,a then e,a then f,a 
*/

% Question 2:

reverse([],A,A).
reverse([Head|Tail],K,T) :- reverse(Tail,K,[Head|T]).
reverse(L,K) :- reverse(L,K,[]).

% Question 3:

follows(anne,fred).
follows(fred,julie).
follows(fred,susan).
follows(john,fred).
follows(julie,fred).
follows(susan,john).
follows(susan,julie).

tweeted(tweet1,anne).
tweeted(tweet2,fred).
tweeted(tweet3,john).
tweeted(tweet4,john).
tweeted(tweet5,anne).
tweeted(tweet6,julie).
tweeted(tweet7,fred).
tweeted(tweet8,fred).
tweeted(tweet9,susan).
tweeted(tweet10,susan).

%Assume people can see thier own tweets.
see(Person,Tweet) :- tweeted(Tweet,Person).
see(Person,Tweet) :- follows(Person,X) , tweeted(Tweet,X).
friends(X,Y) :- follows(X,Y) , follows(Y,X).

seeRetweet(Person,Tweet) :- see(Person,Tweet).
seeRetweet(Person,Tweet) :- follows(Person,X), see(X,Tweet). 

%Question 4:


add_up_list([],[],_).
add_up_list([Head|Tail],[Head2|Tail2],N) :- RunningTotal is Head + N, Head2 = RunningTotal, add_up_list(Tail,Tail2,Head2).  
add_up_list(L,K) :- add_up_list(L,K,0).

%Question 5:

nationality(malcolm, scottish).
nationality(claude, french).
nationality(john, british).
nationality(owen, welsh).
nationality(sean, northernIrish).
nationality(nigel, english).
nationality(X,british) :- nationality(X,scottish).
nationality(X,british) :- nationality(X,english).
nationality(X,british) :- nationality(X,welsh).
nationality(X,british) :- nationality(X,northernIrish).

hobby(malcolm, rugby).
hobby(claude, football).
hobby(john, cricket).
hobby(owen, chess).
hobby(nigel, football).

sportsman(X) :- hobby(X,cricket).
sportsman(X) :- hobby(X,rugby).
sportsman(X) :- hobby(X,football).

/**
*1 ?- sportsman(owen).
*false.
*
*3 ?- findall(X,(sportsman(X), nationality(X,british)),BritishSportsmen).
*BritishSportsmen = [john, malcolm, nigel].
*
*4 ?- findall(Y,(nationality(X,Y),hobby(X,football)),FootballingNationalities).
*FootballingNationalities = [french, english, british].
*/

%Question 6:
%Assumed order is ascending
merge([],[],[]).
merge([H1,T1],[],[H3|T3]) :- H1 = H3 , merge(T1,[],T3).
merge([],[H2|T2],[H3|T3]) :- H2 = H3 , merge([],T2,T3). 
merge([H1|T1],[H2|T2],[H3|T3]) :- H1 < H2 , H3 = H1, merge(T1, [H2|T2], T3).
merge([H1|T1],[H2|T2],[H3|T3]) :- H2 < H1 , H3 = H2, merge([H1|T1], T2, T3).

%Question 7:

gcd(A,0,C) :- C = A.
gcd(A,B,C) :- A =< B, Diff is B - A, gcd(A,Diff,C). 
gcd(A,B,C) :- A > B, gcd(B,A,C).

%Question 8:

%Test tree

consbt(a,b,c).
consbt(b,d,e).
consbt(c,f,g).
consbt(d,emptybt,emptybt).
consbt(e,emptybt,emptybt).
consbt(f,emptybt,emptybt).
consbt(g,emptybt,emptybt).

%part a) Pre order

preorder(emptybt,[]).
preorder(Tree,[H|T]) :- consbt(Tree,Left,Right), H = Tree, preorder(Left,LeftTree), preorder(Right,RightTree), append(LeftTree,RightTree,T). 

append([],[],[]).
append([],[H|T],[H3|T3]) :- H3 = H, append([],T,T3).
append([H|T],L,[H3|T3]) :- H3 = H, append(T,L,T3). 





