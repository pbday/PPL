% Problem #1, � I t � s a t i e � , D e l l L og ic P u z zl e s , Oc tober 1999
% Each man (mr so-and-so ) g o t a t i e from a r e l a t i v e .
tie(cupids).
tie(happyfaces).
tie(leprechauns).
tie(reindeer).
mr(crow).
mr(evans).
mr(hurley).
mr(speigler).
relative(daughter).
relative(fatherinlaw).
relative(sister).
relative(uncle).
solve:-
tie(CrowTie),tie(EvansTie),tie(HurleyTie),tie(SpeiglerTie),
alldifferent([CrowTie,EvansTie,HurleyTie,SpeiglerTie]),
relative(CrowRelative),relative(EvansRelative),
relative(HurleyRelative),relative(SpeiglerRelative),
alldifferent([CrowRelative , Evan sRela tive , H u rl e yR ela ti v e ,
S p e i g l e r R e l a t i v e ] ) ,
T r i p l e s = [ [ crow , CrowTie , C rowRela tive ] ,
[ evans , EvansTie , E van sRela ti ve ] ,
[ hu rle y , HurleyTie , H u rl e y R ela ti v e ] ,
[ s p e i g l e r , S p ei gl e r Ti e , S p e i g l e r R e l a t i v e ] ] ,
% 1 . The t i e w i t h t h e g r i n n i n g l e p r e c h a u n s wasn � t
% a p r e s e n t from a d a u g h t e r .
\+ member ( [ , l e p r e c ha u n s , daugh te r ] , T r i p l e s ) ,
% 2 . Mr . Crow � s t i e f e a t u r e s n e i t h e r t h e d anc ing
% r e i n d e e r nor t h e y e l l o w happy f a c e s .
\+ member ( [ crow , r ei n d e e r , ] , T r i p l e s ) ,
\+ member ( [ crow , ha p p y fa c e s , ] , T r i p l e s ) ,
% 3 . Mr . S p e i g l e r � s t i e wasn � t a p r e s e n t from h i s u n cl e .
\+ member ( [ s p e i g l e r , , u n cl e ] , T r i p l e s ) ,