

child_of(les,maurice).
child_of(irene,maurice).
child_of(margaret,irene).
child_of(margaret,george).
child_of(elizabeth,irene).
child_of(elizabeth,george).
child_of(becca,margaret).
child_of(louise,margaret).
child_of(nick,margaret).
child_of(becca,peter).
child_of(louise,peter).
child_of(nick,peter).
child_of(emma,frank).
child_of(harold,frank).
child_of(amelia,frank).
child_of(chris,amelia).
child_of(chris,john).
child_of(brendon,chris).
child_of(brendon,elizabeth).
child_of(emlyn,chris).
child_of(emlyn,elizabeth).

male(maurice).
male(les).
male(george).
male(peter).
male(nick).
male(frank).
male(harold).
male(john).
male(chris).
male(brendon).
male(emlyn).

female(irene).
female(margaret).
female(louise).
female(becca).
female(emma).
female(amelia).
female(elizabeth).

grandparent_of(X,Y) :- child_of(X,Parent), child_of(Parent,Y).
daughter_of(X,Y) :- child_of(X,Y), female(X).
sister_of(X,Y) :- child_of(X,Z), child_of(Y,Z), X \== Y, female(X).
uncle_of(X,Y) :- child_of(Y,Z), child_of(Z,A), child_of(X,A), Z \== X, male(X).
father(X,Y) :- male(X) , child_of(Y,X). 
grandfather(X,Y) :- grandparent_of(X,Y), male(X).

