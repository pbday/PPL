
/**
* Produces a balanced binary search tree given a list of integers. 
*
* Repeat values branch left, although if there are many repeat values a balanced tree may not be possible and fail will be returned.
*/



search_tree(L,T) :- validTree(L,T) , balancedTree(T).

validTree([],emptybt).
validTree(L,T) :- inList(L,N), splitList(L,N,LowerList,HigherList), T = constbt(N,Left,Right), validTree(LowerList,Left) , validTree(HigherList, Right). 

inList([H|_],N) :- H = N.
inList([_|T],N) :- inList(T,N).
inList([],_) :- fail.


balancedTree(T) :- treeDepth(T,Max,Min), Diff is Max - Min, Diff >= 0, Diff =< 1.
treeDepth(T,Max,Min) :- T = constbt(_, Left, Right), treeDepth(Left,MaxL,MinL), treeDepth(Right,MaxR, MinR), max(MaxL,MaxR,MaxD), min(MinL,MinR,MinD), Max is MaxD + 1, Min is MinD + 1.
treeDepth(emptybt,0,0).

max(A,B,C) :- A >= B, C = A.
max(A,B,C) :- A < B, C = B.

min(A,B,C) :-  A >= B, C = B.
min(A,B,C) :-  A < B, C = A.

%Splitting a list into higher and lower, removing first occurance of checking constraint (to avoid removing duplicates)
%splitList requires that theychecking constraint features in the list at least once, this will make it more useful for bst.
splitList([H|T],N,Low,High) :- H = N , splitListNoH(T,N,Low,High).  
splitList([H|T],N,Low,[HH|HT]) :- H > N , HH = H, splitList(T,N,Low,HT).
splitList([H|T],N,[LH|LT],High) :- H < N , LH = H, splitList(T,N,LT,High).

%Adding values equal to checking constraint once first occurance of splitting value has been found
splitListNoH([],_,[],[]).
splitListNoH([H|T],N,Low,[HH|HT]) :- H >= N, HH = H, splitListNoH(T,N,Low,HT).
splitListNoH([H|T],N,[LH|LT],High) :- H < N, LH = H, splitListNoH(T,N,LT,High).
