
//The following is the Scala labsheet 2, functional scala

//1)Write the following anonymous functions in scala

//a) Takes a boolean and returns it

val oneA =
  (x: Boolean) => x
oneA(false)

//b) takes an int arg x and returns a function that takes no arguments which returns x
val oneB =
  (x: Int) =>  () => x

val oneBFunc = oneB(5)
oneBFunc()

//c) A function that takes two arguments, an Int n and a function f from int to int, and applies f to n

val oneC = (f: Int => Int, x: Int) => f(x)

oneC(x => 2*x,5)

//d) The curried version of c
//Awaiting how to curry

//e) Function that takes an int n and two functions f and g from int to int,
//and applies f to the result of g applied to n

val oneE = (f:Int => Int, g:Int =>Int, x:Int)
  => f(g(x))

oneE(x=>2*x,x=>x+1,5)

//f) Curried version of e
//Awaiting how to curry

//2) Write scala code that does the following:
//a) Creates a List containing the integers 1 through 5, in order

val l=List.range(1, 6)

//b) reverse the list in a
val r = l.reverse

//c) returns each element in a negated

val neg = l map (x => -x)

//d) Sums the elements of the list in c

val sumlist = neg.fold(0) {(z:Int,i:Int) =>  z + i}


//3) a) Fibonnacci

 def fib2(n:Int): (Int,Int) = {
   n match {
     case _ if n<2 => (1,1)
     case _ =>
       val prev = fib2(n-1)
       (prev._1 + prev._2, prev._1)
   }
 }

fib2(0)
fib2(1)
fib2(2)
fib2(3)


//3) b) Tail recursive fibbonacci

//Previous example not tail recursive as it requires stroring previous values i nthe stack

def fib3(n:Int, cur:Int, prev:(Int,Int)): (Int,Int) = {
  n match 
  }
}
