val x = 1

def doTwice(f: Int => Int, n: Int) = f(f(n))

val oneA =
  (x: Boolean) => x

val oneB =
  (x: Int) =>  () => x

val oneC = (f: Int => Int, x: Int) => f(x)


oneA(false)

val func = oneB(10)
func()

oneC(x => 2*x,5)



def sum(f: Int => Int)
: (Int, Int) => Int = {

  def sumF(a:Int, b:Int) : Int = {
    if (a>b) 0
    else f(a) + sumF(a+1,b)
  }
  sumF

}

def sumInts = sum(x => x)

sumInts(1,5)