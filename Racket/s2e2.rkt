#lang racket

;Sheet 2 exercise 2

; The expression evaluates to 5. This is because the x in the lamda expression looks up the value in
; the lexical environemnt where the lambda expression is defined, so changing x after that does nothing

(let ((x 2))
  (let ((f (lambda (n) (+ x n))))
    (let ((x 17))
      (f 3))))

;
