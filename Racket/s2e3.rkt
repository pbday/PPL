#lang racket

;Sheet 2 exercise 3

;This evaluates to 103. The n to add is set within the addit definition to 3.
; 

(define (addN n)
  (lambda (m) (+ m n)))

(let* ( (m 10) (n 20) (addit (addN 3)) )
 (addit 100) )
