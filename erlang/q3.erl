
-module(q3).
-export([increasing/1,changePairs/1]).

isInc({X,Y}) when X < Y -> true;
isInc(_) -> false.

increasing(L) -> incAux(L,[]).

incAux([],L) -> L;
incAux([H|T],L) -> 	V = isInc(H),
					if (V) -> incAux(T,[H|L]);
					true -> incAux(T,L)
					end.
					
max2nd([{_,Y}|T]) -> max(Y,max2nd(T));
max2nd(_) -> 0.

changePairs(L) -> M = max2nd(L),
						cpAux(L,M).
						
cpAux([{X,_}|T],M) -> [{X,M} | cpAux(T,M)];
cpAux([],_) -> []. 



