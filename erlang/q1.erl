
-module(q1).
-export([salesA/1,salesB/2,allZeroPeriod/1,maxUpTo/1]).

%(a) Define a function which given a week number n returns the number of weeks in the
%range 0,...,n for which the sales were 0.

salesA(X) when X >= 0 -> rem0(X) + salesA(X-1);
salesA(_) -> 0.
	
rem0(X) ->
	if  ((X + 31) rem 7) + ((X + 1) rem 5) == 0 -> 1;
	true -> 0
	end.
%if ((X + 31) rem 7) + ((X + 1) rem 5) == 0 -> .

%salesAux(X,T) ->
%	if (X = -1) -> T;
%	if 

%sales(N) -> (N + 31) rem 7 + (N + 1) rem 5.

%(b) Define a function which given a week number n and a number s returns the number
%of weeks in the range 0,...,n for which the sales where higher than s.

salesB(N,S) when N >= 0 -> gt(N,S) + salesB(N-1,S);
salesB(_,_) -> 0. 

gt(X,S) -> if ((X + 31) rem 7) + ((X + 1) rem 5) > S -> 1;
			true -> 0
			end.

%(c) Define the function allZeroPeriod(N) such that allZeroPeriod tests whether the
%sales for every week in the range 0ton are zero.

allZeroPeriod(N) when N >= 0 -> io:fwrite("Sales 0 in week ~B: ~s ~n",[N, is0(N)]),
								allZeroPeriod(N-1);
allZeroPeriod(_) -> ok.


is0(X) ->
	if  ((X + 31) rem 7) + ((X + 1) rem 5) == 0 -> "True";
	true -> "False"
	end.									

%(d) Define a function which given a week number n finds the week in the range 0,...,n
%which had the maximum sales. Don’t forget that you can define helper functions.

maxUpTo(N)  -> maxAux(N,0).

maxAux(N,X) when N >= 0 -> maxAux(N-1, maxSale(N,X));
maxAux(_,X) -> X.  

maxSale(N,X) -> 
	V = ((N + 31) rem 7) + ((N + 1) rem 5),
	if V > X -> V;
	true -> X
	end.
