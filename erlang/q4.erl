
-module(q4).
-export([start/0,printMsg/0,printMsg/1]).

printMsg() ->
	receive
		X -> printMsg(X)
	end.
	
printMsg(X) -> 
	receive 
		Y -> 	io:format(Y),
				io:format(X)
		end,
		printMsg().
		

start() ->
	register(q4Msg, spawn(q4, printMsg, [])).
