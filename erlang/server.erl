-module(server).
-export([echo/0,returner/0,start/0,sender/1,client/0]).

echo() -> 
	receive 
		S -> 	io:format("Echo: ~p~n",[S])
				end,
		echo().
	
%returner() ->
%	receive
%		{Pid,S} -> Pid ! S
%					end,
%		returner().

returner() ->
	receive
		{Pid, N} -> Pid ! fact(N)
					end,
		returner().

fact(1) -> 1;
fact(N) -> N * fact(N-1).

sender(S) ->
	returner ! {self(),S},
	receive
		Rtn -> io:format("Return: ~p~n",[Rtn]) 
				end.

client() ->
	returner ! {self(),5},
	receive
		N -> io:format("5 factorial is: ~p~n",[N])
				end,
	returner ! {self(),7},
	receive 
		N2 -> io:format("7 factorial is: ~p~n",[N2])
				end.
		
start() ->
	register(echo, spawn(server, echo, [])),
	register(returner, spawn(server, returner, [])).