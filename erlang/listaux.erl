-module(listaux).
-export([min/1,max/1,minmax/1]).

checkmin(X,Y) -> if (X > Y) -> Y;
					true -> X
					end.
					
min([H|T]) -> lists:foldl(fun checkmin/2,H, [H|T]).

checkmax(X,Y) when X > Y -> X;
checkmax(_,Y) -> Y.

max([H|T]) -> lists:foldl(fun checkmax/2, H, [H|T]).

minmax(L) -> {min(L),max(L)}.